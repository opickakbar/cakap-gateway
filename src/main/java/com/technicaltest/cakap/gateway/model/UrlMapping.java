package com.technicaltest.cakap.gateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UrlMapping implements Serializable {

    @JsonProperty("service")
    private String service;

    @JsonProperty("url")
    private String url;

    @JsonProperty("authenticated")
    private Boolean authenticated;

}
