package com.technicaltest.cakap.gateway.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technicaltest.cakap.gateway.repository.ConfigDataRepository;
import com.technicaltest.cakap.gateway.service.UrlMappingService;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

@Controller
@Slf4j
public class GatewayController {

    @Autowired
    private UrlMappingService urlMappingService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ConfigDataRepository configDataRepository;

    @RequestMapping(value = "/{service}/**")
    public @ResponseBody ResponseEntity<Object> forward(@PathVariable String service, RequestEntity<Object> requestEntity) throws JsonProcessingException {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                    .getRequest();
            log.info("REQUEST : {}", request);
            ResponseEntity<Object> responseEntity = urlMappingService.forwardRequest(service, requestEntity);
            log.info("RESPONSE : {}", responseEntity);
            return responseEntity;
        } catch (RestClientResponseException re) {
            log.error("Forward request to failed, error = {}", re.getMessage());
            log.error("RESPONSE ERROR :: > MESSAGE:" + re.getLocalizedMessage());
            return handleForwardError(re);
        } catch (CommonException ce) {
            ErrorResponse response = ErrorResponse.builder()
                    .status(false)
                    .responseMessage(ce.getMessage())
                    .build();
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorResponse response = ErrorResponse.builder()
                    .status(false)
                    .responseMessage(e.getMessage())
                    .build();
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity<Object> handleForwardError(RestClientResponseException re) throws JsonProcessingException {
        HttpStatus httpStatus = Optional.ofNullable(HttpStatus.resolve(re.getRawStatusCode()))
                .orElse(HttpStatus.INTERNAL_SERVER_ERROR);
        Object responseObject = objectMapper.readValue(re.getResponseBodyAsString(), Object.class);
        Map<String, String> response = objectMapper.convertValue(responseObject, Map.class);
        return new ResponseEntity<>(response, httpStatus);
    }

}
