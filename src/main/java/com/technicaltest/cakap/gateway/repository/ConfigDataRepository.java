package com.technicaltest.cakap.gateway.repository;

import com.technicaltest.cakap.gateway.dto.ConfigData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigDataRepository extends JpaRepository<ConfigData, Long> {
    ConfigData findByKey(String key);
}
