package com.technicaltest.cakap.gateway.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.technicaltest.common.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Component
@Slf4j
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Autowired
    private ObjectMapper objectMapper;

    private ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler();

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
        ErrorResponse errorResponse = objectMapper.readValue(clientHttpResponse.getBody(), ErrorResponse.class);
        log.info(errorResponse.toString());
    }
}
