package com.technicaltest.cakap.gateway.proxy;

import com.technicaltest.common.response.ConfigResponse;

public interface UrlMappingProxy {
    ConfigResponse get(String key);
}
