package com.technicaltest.cakap.gateway.proxy.impl;

import com.technicaltest.cakap.gateway.dto.ConfigData;
import com.technicaltest.cakap.gateway.proxy.UrlMappingProxy;
import com.technicaltest.cakap.gateway.repository.ConfigDataRepository;
import com.technicaltest.common.response.ConfigResponse;
import io.lettuce.core.RedisCommandExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class UrlMappingProxyImpl implements UrlMappingProxy {

    @Autowired
    private ConfigDataRepository configDataRepository;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Value("${redis.url.mapping.ttl}")
    private Integer ttl;

    @Override
    public ConfigResponse get(String key) {
        ConfigResponse configResponse = null;
        try {
            configResponse = (ConfigResponse) redisTemplate
                    .boundValueOps(key).get();
        } catch (RedisConnectionFailureException | RedisCommandExecutionException e) {
            log.error("Failed to connect to redis: {}", e.getMessage());
        }
        if (configResponse != null) {
            log.info("Get config data from redis, configResponse = {}", configResponse);
            return configResponse;
        }
        ConfigData configData = configDataRepository.findByKey(key);
        if (Objects.isNull(configData)) {
            return null;
        }
        configResponse = new ConfigResponse(configData.getKey(), configData.getValue());
        log.info("Get config data from database, configResponse = {}", configResponse);
        try {
            BoundValueOperations<String, Object> boundValueOperations = redisTemplate.boundValueOps(key);
            boundValueOperations.set(configResponse);
            boundValueOperations.expire(ttl, TimeUnit.MILLISECONDS);
        } catch (RedisConnectionFailureException | RedisCommandExecutionException e) {
            log.error("Failed to connect to redis: {}", e.getMessage());
        }
        return configResponse;
    }
}
