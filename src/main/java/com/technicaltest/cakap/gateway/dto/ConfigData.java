package com.technicaltest.cakap.gateway.dto;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = "configs")
@NoArgsConstructor
@AllArgsConstructor
public class ConfigData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "config_generator")
    @SequenceGenerator(
            name = "config_generator",
            sequenceName = "config_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column(name = "key")
    private String key;

    @Column(name = "value", columnDefinition = "text")
    private String value;
}
