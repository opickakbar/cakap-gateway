package com.technicaltest.cakap.gateway.service;

import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

public interface UrlMappingService {
    ResponseEntity<Object> forwardRequest(String service, RequestEntity<Object> requestEntity);
}
