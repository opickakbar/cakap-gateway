package com.technicaltest.cakap.gateway.service.impl;

import com.technicaltest.cakap.gateway.model.UrlMapping;
import com.technicaltest.cakap.gateway.outbound.AuthOutbound;
import com.technicaltest.cakap.gateway.proxy.UrlMappingProxy;
import com.technicaltest.cakap.gateway.service.UrlMappingService;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.response.ConfigResponse;
import com.technicaltest.common.response.auth.CheckTokenResponse;
import com.technicaltest.common.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class UrlMappingServiceImpl implements UrlMappingService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UrlMappingProxy urlMappingProxy;

    @Autowired
    private AuthOutbound authOutbound;

    private static final String KEY = "GATEWAY_MAPPING_URL";

    @Override
    public ResponseEntity<Object> forwardRequest(String service, RequestEntity<Object> requestEntity) {
        UrlMapping urlMapping = getMapping(service);
        HttpHeaders newHeaders = constructHeaders(requestEntity);
        if (Optional.ofNullable(urlMapping.getAuthenticated()).orElse(false)) {
            CheckTokenResponse checkTokenResponse = authenticate(requestEntity);
            newHeaders.add("role", checkTokenResponse.getRole());
        }
        String path = getPath(service, requestEntity.getUrl().getPath());
        String query = "";
        if (requestEntity.getUrl().getQuery() != null) query = "?" + requestEntity.getUrl().getQuery();
        URI uri = URI.create(urlMapping.getUrl() + path + query);
        RequestEntity<Object> forwardRequestEntity = new RequestEntity<>(requestEntity.getBody(),
                newHeaders, requestEntity.getMethod(), uri);
        ResponseEntity<Object> responseEntity = null;
        log.info("Forward request to service = {} with headers: {} and uri = {}", service, newHeaders, uri.toString());
        responseEntity = restTemplate.exchange(forwardRequestEntity, Object.class);
        log.info("Forward request to service = {} with headers: {} and uri = {} success", service, newHeaders, uri.toString());
        return responseEntity;
    }

    private UrlMapping getMapping(String path) {
        log.info("Get url mapping by path = {}", path);
        ConfigResponse configResponse = urlMappingProxy.get(KEY);
        if (Objects.isNull(configResponse)) {
            throw new CommonException("Url mapping is not set up");
        }
        UrlMapping[] urlMappings = JsonUtil.toObject(configResponse.getValue(), UrlMapping[].class);
        UrlMapping urlMapping = Optional.ofNullable(getUrlMapping(urlMappings, path))
                .orElseThrow(() -> new CommonException("Url not found"));
        log.info("Get url mapping success, urlMapping = {}", urlMapping);
        return urlMapping;
    }

    private UrlMapping getUrlMapping(UrlMapping[] urlMappings, String service) {
        for (UrlMapping urlMapping : urlMappings) {
            if (service.equals(urlMapping.getService())) {
                return urlMapping;
            }
        }
        return null;
    }

    private CheckTokenResponse authenticate(RequestEntity<Object> requestEntity) {
        HttpHeaders httpHeaders = requestEntity.getHeaders();
        List<String> authorizations = Optional.ofNullable(httpHeaders.get("token"))
                .orElseThrow(() -> new CommonException("Token is required"));
        return authOutbound.checkToken(authorizations.get(0));
    }

    private HttpHeaders constructHeaders(RequestEntity<Object> requestEntity) {
        HttpHeaders headers = requestEntity.getHeaders();
        HttpHeaders newHeaders = new HttpHeaders();
        for (Map.Entry<String, List<String>> header : headers.entrySet()) {
            newHeaders.addAll(header.getKey(), header.getValue());
        }
        return newHeaders;
    }

    private String getPath(String service, String originPath) {
        String[] arr = originPath.split(service);
        return arr.length >= 2 ? arr[1] : "";
    }
}
