package com.technicaltest.cakap.gateway.outbound.impl;

import com.technicaltest.cakap.gateway.outbound.AuthOutbound;
import com.technicaltest.common.exception.CommonException;
import com.technicaltest.common.response.auth.CheckTokenResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class AuthOutboundImpl implements AuthOutbound {

    private RestTemplate restTemplate;

    @Value("${outbound.auth.url}")
    private String authServiceUrl;

    public AuthOutboundImpl() {
        this.restTemplate = new RestTemplate();
    }

    @Override
    public CheckTokenResponse checkToken(String token) {
        CheckTokenResponse checkTokenResponse = null;
        try {
            log.info("CheckToken processing with token = {}", token);
            checkTokenResponse = restTemplate.getForObject(authServiceUrl + "/api/check-token/" + token, CheckTokenResponse.class);
            log.info("CheckToken success with response = {}", checkTokenResponse);
        } catch (Exception e) {
            log.error("CheckToken failed with error = {}", e.getMessage());
            throw new CommonException("Token is invalid!");
        }
        return checkTokenResponse;
    }
}
