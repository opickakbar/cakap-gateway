package com.technicaltest.cakap.gateway.outbound;

import com.technicaltest.common.response.auth.CheckTokenResponse;

public interface AuthOutbound {
    CheckTokenResponse checkToken (String token);
}
